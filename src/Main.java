import javax.swing.JFrame;
import java.awt.*;
import javax.swing.JPanel;

public class Main extends JFrame {

    class App extends JPanel {

    
        Grid grid;

        public App(){
                setPreferredSize(new Dimension(720, 720));
                grid = new Grid(); 
            }

            @Override
            public void paint(Graphics g) {

                grid.paint(g);

                if (getMousePosition() != null) {
                    g.setColor(Color.RED);
                    g.fillOval((int) getMousePosition().getX(), (int) getMousePosition().getY(), 5, 5);
                }
            }
        }
    
    public static void main(String[] args) throws Exception {
        Main window = new Main();
        window.run();
    }

    private Main() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        App canvas = new App();
        this.setContentPane(canvas);
        this.pack();
        this.setVisible(true);
    }

    public void run() {
        while (true) {
            this.repaint();
        }
    }
}